include ${FSLCONFDIR}/default.mk

PROJNAME = MVdisc
SOFILES  = libfsl-MVdisc.so
LIBS     = -lfsl-vtkio -lfsl-first_lib

all: libfsl-MVdisc.so

libfsl-MVdisc.so: MVdisc.o
	${CXX} ${CXXFLAGS} -shared -o $@ $^ ${LDFLAGS}
