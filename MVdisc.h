/*    Copyright (C) 2012 University of Oxford  */

/*  CCOPYRIGHT  */

#ifndef _MVdisc
#define _MVdisc

#include <iostream>

#include <string>
#include <fstream>
#include <stdio.h>
#include <algorithm>

#include "armawrap/newmat.h"
#include "newimage/newimageall.h"
#include "meshclass/meshclass.h"


namespace mvdisc{
  class mvdiscException : public std::exception{

  public:
    const char* errmesg;
    mvdiscException(const char* msg)
    {
      errmesg=msg;
    }

  private:
    virtual const char* what() const throw()
    {
      return errmesg;
    }
  };



  class MVdisc {

  public:
    MVdisc();
    ~MVdisc();

    std::vector<unsigned int> applyLDA(const std::vector<NEWMAT::ColumnVector> & Data, const float & eigThresh) const ;
    short applyLDA(NEWMAT::ColumnVector & Data, const float & eigThresh) const;
    NEWMAT::ColumnVector run_LOO_LDA(const NEWMAT::Matrix & Data, const NEWMAT::ColumnVector & target);

    void estimateLDAParams(const NEWMAT::Matrix & Data, const NEWMAT::ColumnVector & target) ;
    void estimateAndAppendLDAParams(const NEWMAT::Matrix & Data, const NEWMAT::ColumnVector & target) ;

    NEWMAT::ReturnMatrix getGroupMeans(const NEWMAT::Matrix & Data, const NEWMAT::ColumnVector & target){  return calculateClassMeans(Data, target, LDAnsubs); }

    //---------------------------------I/O FUNCTION-----------------------------------------//

    void saveLDAParams(const std::string & outname ) const;
    void saveLDAParams(const std::string & outname, const NEWMAT::Matrix & polygons ) const;

    //---------------------------------SETTING PARAMETERS--------------------------------------//
    void set_LDA_Params(const NEWMAT::Matrix & means, const NEWMAT::Matrix & cov_vecs,  const std::vector<float> & cov_eigs,const std::vector<unsigned int> n)
    { LDAmu=means ; LDAcov_Vecs=cov_vecs; LDAcov_Eigs=cov_eigs; LDAnsubs=n; };

    const NEWMAT::Matrix* getLDAcov_Vecs_ptr(){ return &LDAcov_Vecs; }
    std::vector<float>::const_iterator getLDAcov_Eigs_iter(){ return LDAcov_Eigs.begin(); }



  private:

    //------------------------------LDA/QDA parameters----------------------------//
    NEWMAT::Matrix LDAmu; //A column per group
    NEWMAT::Matrix LDAcov_Vecs;//can be shared by with QDA
    std::vector<float> LDAcov_Eigs;//can be shared by with QDA
    std::vector<unsigned int> LDAnsubs;


    void quickSVD(const NEWMAT::Matrix & data,  NEWMAT::DiagonalMatrix &D,NEWMAT::Matrix &U, NEWMAT::Matrix &V ) const ;
    void quickSVD(const NEWMAT::Matrix & data,  NEWMAT::DiagonalMatrix &D,NEWMAT::Matrix &U ) const ;

    NEWMAT::ReturnMatrix calculateClassMeans(const NEWMAT::Matrix & Data, const NEWMAT::ColumnVector & target, std::vector<unsigned int> & nk) const ;
    NEWMAT::ReturnMatrix sortAndDemeanByClass(const NEWMAT::Matrix & Data, const NEWMAT::ColumnVector & target, const NEWMAT::Matrix & muM, const std::vector<unsigned int> & nk,NEWMAT::ColumnVector & targSorted) const;
    void estimateCommonCov(const NEWMAT::Matrix & DeMean, const std::vector<unsigned int> & nK, NEWMAT::Matrix & U, std::vector<float> & D) const;
    void estimateClassCovs(const NEWMAT::Matrix & DeMean, const NEWMAT::ColumnVector & target, const std::vector< int > & nK, std::vector< NEWMAT::Matrix > & vU, std::vector< NEWMAT::DiagonalMatrix > & vD) const;


    template<class T>
    std::vector<T> threshInvert(const std::vector<T> & D, const T & p) const ;
    //discrimiant function

  };
}
#endif
